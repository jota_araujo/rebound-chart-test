# Description
An example of how to apply physics based animation to animate charts on Android.   
Uses Facebook's Rebound  animation library.

# Date
September/2015

# Preview

![preview](https://bytebucket.org/jota_araujo/rebound-chart-test/raw/a37989c935281c172dd3b3e17787f4648ae07805/rebound_animation_android.gif)