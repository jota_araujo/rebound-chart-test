package com.example.joao.reboundbarcharts;

import android.app.Activity;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joao.reboundbarcharts.model.PerformanceResultVO;
import com.example.joao.reboundbarcharts.rest.RestClient;
import com.facebook.rebound.SpringSystem;

import org.apache.commons.lang3.StringUtils;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ChartFragment extends Fragment {

    private static final String PARAM_INDEX = "param_index";

    private int index;
    private ViewGroup rootView;
    private static SpringSystem mSpringSystem;
    private  ExpenseReportBuilder builder;
    private static Typeface tfBold;
    private static Typeface tfRegular;
    private TextView averageText;

    private OnFragmentInteractionListener mListener;


    public static ChartFragment newInstance(int index) {
        ChartFragment fragment = new ChartFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_INDEX, index);
        fragment.setArguments(args);
        return fragment;
    }

    public ChartFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            index = getArguments().getInt(PARAM_INDEX);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = (ViewGroup) inflater.inflate(R.layout.fragment_chart, container, false);

        if( tfBold == null) tfBold = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Bold.ttf");
        if( tfRegular == null) tfRegular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Lato-Regular.ttf");
        TextView footerText = (TextView) rootView.findViewById(R.id.sectionFooterText);
        footerText.setTypeface(tfBold);
        averageText = (TextView) rootView.findViewById(R.id.weeklyAverageLabel);
        averageText.setTypeface(tfRegular);

        if(mSpringSystem == null) mSpringSystem = SpringSystem.create();


        requestPerformanceData();

        return  rootView;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    private void requestPerformanceData(){

            RestClient.instance().getService().getPerformance(index, new Callback<PerformanceResultVO>() {

            @Override
            public void success(PerformanceResultVO result, Response response) {

                try {

                    String[] arr = result.balanceHistory.split(",");
                    int[] arr2 = new int[5];
                    for (int i = 0; i <arr.length; i++) {
                        int n =  Integer.parseInt(StringUtils.trim(arr[i]));
                        arr2[i] = (int) Math.round(n*0.01);

                    }

                    builder = new ExpenseReportBuilder(mSpringSystem);
                    builder.build(arr2, (ViewGroup) rootView.findViewById(R.id.dotted_line_container),
                            (ViewGroup) rootView.findViewById(R.id.bar_group), averageText);

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Erro", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void failure(RetrofitError error) {

                try {
                    Toast.makeText(getActivity(), "Falha no carregamento de dados", Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }

        });
    }



}
