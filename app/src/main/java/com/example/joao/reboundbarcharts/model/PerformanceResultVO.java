package com.example.joao.reboundbarcharts.model;

import java.util.Date;

/**
 * Created by joao on 26/10/15.
 */
public class PerformanceResultVO {
    public int goal;
    public String balanceHistory;
    public String date;
}
