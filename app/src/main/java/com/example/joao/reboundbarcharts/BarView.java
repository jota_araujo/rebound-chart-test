package com.example.joao.reboundbarcharts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.joao.reboundbarcharts.util.Utils;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringListener;
import com.orhanobut.logger.Logger;

import java.util.Random;

/**
 * Created by joao on 25/10/15.
 */
public class BarView extends View implements SpringListener {


    public Spring mSpring;
    private TextView mLabel;
    private View mSpill;
    private int limit;
    private static Typeface tf;

    public BarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public void onStartUp(Spring spring, int color, int targetValue){

        mSpring = spring;
        setBackgroundColor(color);


       if(tf == null) tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Lato-Regular.ttf");


        this.mSpill = ((ViewGroup) getParent()).findViewById(R.id.bar_spill);
        this.mLabel = (TextView) ((ViewGroup) getParent()).findViewById(R.id.bar_label);
        mLabel.setTypeface(tf);

        int targetHeight = Utils.dpToPx(getContext(), targetValue);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) getLayoutParams();
        params.height = targetHeight;
        setLayoutParams(params);
        setPivotY(targetHeight);

        limit = Utils.dpToPx(getContext(), 100);

        mLabel.setText("R$ " + String.valueOf(targetValue));
        mSpill.setY(limit);

    }

    public void activate(){

        mSpring.setCurrentValue(1f);
        setVisibility(View.VISIBLE);
        mLabel.setVisibility(View.VISIBLE);
        mSpring.setEndValue(0f);

    }

    private int nextPass = 0;
    private int counter = 0;


    @Override
    public void onSpringUpdate(Spring spring) {
        float value = (float) spring.getCurrentValue();

        float scale = 1f - (value * 0.9f);
        setScaleY(scale);
        float aHeight =  getHeight() * scale;
        float aTop = getBottom() - aHeight;
        float px = getBottom() - aHeight - mLabel.getHeight();
        mLabel.setY(px);
        //Logger.e(mSpill.getTop() + ""  + mSpill.getBottom());
        if( aTop <= limit){
            mSpill.setY((int) aTop);
            mSpill.setBottom((int) (limit - aTop));
            if(nextPass == 3){
                mSpill.setAlpha(1.0f);
                nextPass = 4;
            }
            if(nextPass <3){

                nextPass++;
            }
        }else{
            if(mSpill.getAlpha() > 0) mSpill.setAlpha(0);
        }

        if(counter % 10 == 0){
            //mLabel.setText("R$ " + String.valueOf(Math.round(aHeight)));
        }
        counter++;
    }

    @Override
    public void onSpringAtRest(Spring spring) {
        //Logger.e("at rest...");

    }

    @Override
    public void onSpringActivate(Spring spring) {



    }

    @Override
    public void onSpringEndStateChange(Spring spring) {
       // Logger.e("end state change");

    }


}
