package com.example.joao.reboundbarcharts;

import android.content.res.Resources;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.example.joao.reboundbarcharts.model.PerformanceResultVO;
import com.example.joao.reboundbarcharts.util.Utils;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringSystem;

import java.util.Random;

/**
 * Created by joao on 26/10/15.
 */
public class ExpenseReportBuilder {

    private SpringSystem mSpringSystem;

    public BarView[] bars;
    //public int[] rands;
    private PerformanceResultVO data;

    private static double TENSION = 250;
    private static double DAMPER = 20; //friction

    public ExpenseReportBuilder(SpringSystem mSpringSystem) {
        this.mSpringSystem = mSpringSystem;
        bars = new BarView[5];
        //rands = new int[5];
    }

    public void build( int[] rands, ViewGroup mDottedContainer, ViewGroup mContainer, TextView mAverageLabel){

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mDottedContainer.getLayoutParams();
        params.height = Utils.dpToPx(mDottedContainer.getContext(), 100 + 1);
        mDottedContainer.setLayoutParams(params);

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) mContainer.getLayoutParams();
        params2.height = Utils.dpToPx(mContainer.getContext(), 200);
        mContainer.setLayoutParams(params2);

        int sum = 0;
        for (int i = 0; i < 5; i++) {
            //rands[i] =  Math.
            sum += rands[i];
        }
        // TODO: use a currency formatter instead
        mAverageLabel.setText("R$ " + String.valueOf(Math.round(sum/5)));


        Resources res = mContainer.getResources();
        bars[0] = getBar(mContainer.findViewById(R.id.week1), res.getColor(R.color.expense_bar_dark), rands[0]);
        bars[1] = getBar(mContainer.findViewById(R.id.week2), res.getColor(R.color.expense_bar_light), rands[1]);
        bars[2] = getBar(mContainer.findViewById(R.id.week3), res.getColor(R.color.expense_bar_dark), rands[2]);
        bars[3] = getBar(mContainer.findViewById(R.id.week4), res.getColor(R.color.expense_bar_light), rands[3]);
        bars[4] = getBar(mContainer.findViewById(R.id.week5), res.getColor(R.color.expense_bar_current), rands[4]);

        final int delay = 300;
        for (int i = 0; i < 5; i++) {
            Handler handler = new Handler();
            handler.postDelayed(new BarRunnable(bars[i]), delay + i*150);
            //bars[i].activate();
        }

    }

    private BarView getBar(View view, int color, int targetValue){

        Spring mSpring = mSpringSystem.createSpring();
        mSpring.setCurrentValue(1f);

        SpringConfig config = new SpringConfig(TENSION, DAMPER);
        mSpring.setSpringConfig(config);

        BarView barView = (BarView) ((ViewGroup)view).findViewById(R.id.bar_base);
        barView.onStartUp(mSpring, color, targetValue);
        mSpring.addListener(barView);

        return barView;
    }


    public class BarRunnable implements Runnable {
        private BarView barView;
        public BarRunnable(BarView barView) {
            this.barView = barView;
        }

        public void run() {
            barView.activate();
        }
    }




}
