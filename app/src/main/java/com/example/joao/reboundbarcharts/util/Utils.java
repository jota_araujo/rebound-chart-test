package com.example.joao.reboundbarcharts.util;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by joao on 26/10/15.
 */
public class Utils {

    public static int dpToPx(Context ctx, int dp) {
        DisplayMetrics displayMetrics = ctx.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
}
