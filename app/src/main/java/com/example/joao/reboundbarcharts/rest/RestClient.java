package com.example.joao.reboundbarcharts.rest;

/**
 * Created by joao on 26/10/15.
 */
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

public class RestClient {

    private RestAdapter adapter;
    private RestService service;

    public RestClient(){

        RequestInterceptor requestInterceptor;
        requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                // TODO:  add security token param here
                request.addHeader("Accept", "application/json" );
            }
        };

        /*Gson gson = new GsonBuilder()
                .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                .create();*/

        adapter = new RestAdapter.Builder()
                .setEndpoint("http://myflaskapp-smartside.rhcloud.com/api")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(requestInterceptor)
                        //.setConverter(new GsonConverter(gson))
                .build();

        service = adapter.create(RestService.class);
    }

    public RestService getService(){
        return service;
    }


    private static RestClient mInstance;

    public static RestClient instance(){
        if(mInstance == null)
        {
            mInstance = new RestClient();
        }
        return mInstance;
    }

}
