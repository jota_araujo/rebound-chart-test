package com.example.joao.reboundbarcharts.rest;


import com.example.joao.reboundbarcharts.model.PerformanceResultVO;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface RestService {

    @GET("/v1.0/performance/{index}")
    void getPerformance(@Path("index") int index, Callback<PerformanceResultVO> callback);

}